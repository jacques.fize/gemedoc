# About Gemedoc

## What is Gemedoc ?

Gemedoc *[jĕmĕdoc]*, built on the words: *gémeau* (twins) and *document*, is a web platform for document similarity annotation.

Gemedoc is using [**Flask**](http://flask.pocoo.org/) on the server-side and [**Bootstrap**](http://getbootstrap.com/) + [**FontAwesome**](http://fontawesome.io/) for the webdesign. We also use [**Plotly.js**](https://plot.ly/javascript/) for the graphs and obviously **JQuery**.

<div class="col-lg-12">
  <img class="img-responsive col-lg-10 col-lg-offset-1" src="{{url_for('static', filename='img/interface_2.png')}}" alt="">
  <p class="col-lg-12 text-center"><em>Figure 1: Main Interface</em></p>
</div>

## Why Gemedoc ?

In our work, we are interested in text matching i.e. find correspondancies between documents. In addition, we want this correspondancies to be different depending on which caracteristic you compare the documents. For us, we would like to extract correspondancies using their **thematic** and **spatiality**.

Developing such methods came with rich contributions such as : knowledge discovery, corpus cartography, migration analysis, epidemic surveillance, etc.

To conceive and evaluate our method, we need an annotated corpora which gives us this information. Unfortunately, at the best of our knowledge, such a corpus does not exists<sup data-text="Or maybe the PAN09 corpus is the closest corpus">1</sup>. Thus, we develop **Gemedoc**.


## Who is behind Gemedoc ?

This software was designed by: [Jacques Fize](http://jacquesfize.com) (*developer*),  [Mathieu Roche](http://textmining.biz/Staff/Roche/MR/) and [Maguelonne Teisseire](http://textmining.biz/Staff/Teisseire/).
