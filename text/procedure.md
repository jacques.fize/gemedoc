# Documentation

## Why Gemedoc ?

The aim of Gemedoc is to build a corpus to evaluate text-matching algorithms on different dimensions in text: **thematic**, **spatiality** and **temporality**.  
Hence, each pair of documents can be associated different similarity degrees. Each degree indicates the similarity between the two document according to one dimension **d**. For example, a pair of document d1 and d2 will be associated with:

*   _spat(doc1,doc2)_ = weak similarity
*   _them(doc1,doc2)_ = good similarity

To define the similarity degrees between documents, we develop this tool to gather different similarity annotations between documents in our corpus. These evaluations will be merged to obtain objective similarity indicators between documents.

## How to annotate ?

### The intuition

**For each pair of document**, you must **assign a similarity degree** **for each dimension**. Each degree is explained in the _Similarity Scale_ inset.  

> How do we define what makes two documents similar (spatialy, thematicaly, ...)?

**To be honest, it's (praticaly) completely up to you !** Not giving you specific indices of comparison, allow us to have your **unbiased point of view**.


#### Thematic similarity
We defined the thematic similarity as the similarity between words or words pattern in the compared documents. To do that, there is different approach such as extracting keywords, find the general topics (see Figure 1)

#### Spatial Similarity
Compared to the thematic similarity between documents, spatiality in a text can be expressed in different manners. For example, we can be interested by:

 * (if there is) The narrator position
 * Mentionned spatial entities in the documents
 * Movement Pattern


##### Spatial indicator
<blockquote>
”[...]Alex Yallop/MSF Portrait of Rula and Fahad from <font color="red">Syria</font>. \"We <font color="blue">left</font> <font color="red">Aleppo</font> two months ago. The journey was really difficult. Now here in <font color="red">Idomeni</font> we don’t have a place to stay.It is really painful to be here, without protection. We feel that our rights are not respected.\" The couple <font color="blue">arrived to</font> the MSF clinic at <font color="red">Idomeni camp</font> but Rula was later taken to <font color="red">Kilkis hospital</font> and gave birth to baby Abdulrahman [...]

<br><br>
*Source: MSF*
</blockquote>


In the text below, as factor of comparisons, we can use :

 * Narrator position : <font color="red">Idomeni camp</font>
 * Spatial entities : <font color="red">Syria</font>, <font color="red">Aleppo</font>, <font color="red">Idomeni camp</font>, etc..
 * Trajectories: * Aleppo --> Idomeni --> Kilkis hospital*

### Similarity Scale

We defined 4 similarity degrees:

*   **Don't Know** You can't evaluate the similarity between the documents (according to a dimension d)
*   **Different** The two documents are <span class="diff">totaly different</span>(according to a dimension d)
*   **Similar** The two documents share a <span class="sim">large set of similaritites</span> with <span class="diff">few differences</span> (according to a dimension d)
*   **Very Similar** The two documents are <span class="sim">strongly similar</span> except <span class="diff">tiny differences</span> (according to a dimension d)

## Use Gemedoc

### Account Creation

To be an annotator, you need to be registered in the user database of Gemedoc. To obtain an account and become an annotator send us an email with you <mark>name</mark> and <mark>email</mark> (used for login) at:

**jacques[dot]fize[at]cirad[dot]fr**

Once your account created, you'll receive an email with your password to access Gemedoc platform.

### Annotate a corpus

First, you need to select a corpus, using the <kbd>Select Corpus</kbd> button. Then a dialog window appear asking wich corpus you want to use (Figure 1).

<div class="col-lg-12">![]({{url_for('static', filename='img/interface_select_corpus.png')}})

_Figure 1: Corpus Selection dialog_

</div>

Then, you're redirected to the main page (Figure 2).  

<div class="col-lg-12">![]({{url_for('static', filename='img/interface.png')}})

_Figure 2: Main Interface_

</div>

#### Annotate a pair a document

Once, you selected the corpus, a pair a document is displayed at the right. Read the documents to assess their similarities ( _resp. differences_). Then, indicate the different similarity degrees in the annotation panel (Figure 3).

<div class="col-lg-12">![]({{url_for('static', filename='img/tuto_image_5.png')}})

_Figure 3: Annotation panel_

</div>

Once, it's done get to the next pair of document by clicking on the <kbd>Next Annotation</kbd> button.
