# -*- coding: utf-8 -*-
import numpy as np
from .data_manipulation import *
from scipy.stats import friedmanchisquare
from sklearn.metrics import cohen_kappa_score

def fleiss_kappa(*args):
    M=fleiss_kappa_matrix(*args)
    return fleiss_kappa_calculus(M)

def fleiss_kappa_calculus(M):
  """
  See `Fleiss' Kappa <https://en.wikipedia.org/wiki/Fleiss%27_kappa>`_.

  :param M: a matrix of shape (:attr:`N`, :attr:`k`) where `N` is the number of subjects and `k` is the number of categories into which assignments are made. `M[i, j]` represent the number of raters who assigned the `i`th subject to the `j`th category.
  :type M: numpy matrix
  """
  N, k = M.shape  # N is # of items, k is # of categories
  n_annotators = float(np.sum(M[0, :]))  # # of annotators

  p = np.sum(M, axis=0) / (N * n_annotators)
  P = (np.sum(M * M, axis=1) - n_annotators) / (n_annotators * (n_annotators - 1))
  Pbar = np.sum(P) / N
  PbarE = np.sum(p * p)

  kappa = (Pbar - PbarE) / (1 - PbarE)

  return kappa

def cohen_kappa(mat1,mat2):
    return cohen_kappa_score(mat1.flatten(),mat2.flatten())


def friedman(*args):
    if len(args) >=3:
        mat=[a.flatten() for a in args]
        return friedmanchisquare(*mat)
    else:
        return None
