# -*- coding: utf-8 -*-

import numpy as np


def transform2matrix(mat,dim):
    """
    Transform multi-dimensional annotation matrix in a one-dimension annotation matrix
    """
    import numpy as np
    new_=np.zeros((len(mat),len(mat)))
    for d in range(len(mat)):
        for c in range(len(mat)):
            new_[d,c]=mat[d][c][dim]
    return new_

def fleiss_kappa_matrix(*args):
    d=np.zeros((len(args[0].flatten()),4))
    for m in args:
        data=m.flatten().astype(int)
        for i in range(len(data)):
            d[i,data[i]]+=1
    return d
