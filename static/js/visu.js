/*
Contains all the graphics functions
*/

function similarity_user_map(data,id) {
  var layout = {
    margin: {
      l: 20,
      r: 0,
      b: 25,
      t: 0,
      pad: 4
    }
  };
  var d_ = [
  {
    z: data,
    type: 'heatmap',
    colorscale: [
      ['0.0', '#BDC3C7'],
      ['0.33', '#F5D76E'],
      ['0.66', '#F89406'],
      ['1', '#C0392B']
    ],
    showscale: false
  }
  ];
  var d3 = Plotly.d3;
  var gd = d3.select("#"+id).node();
  Plotly.newPlot(id, d_, layout);
  return gd

}
