
// $("#TP .btn." + scale[data[curr_doc[0]][curr_doc[1]]['TP']]).addClass("activee");
$("#TH .btn." + scale[data[curr_doc[0]][curr_doc[1]]['TH']]).addClass("activee");
$("#SP .btn." + scale[data[curr_doc[0]][curr_doc[1]]['SP']]).addClass("activee");

$(function() {
  function update_val() {
    var vth = $("div #TH \> .activee").attr("val");
    var vsp = $("div #SP \> .activee").attr("val");

    var old_val = data[curr_doc[0]][curr_doc[1]]["{{ty}}"];
    var new_val = parseInt($("div #{{ty}}\> .activee").attr("val"));
    if (old_val != new_val) {
      $("#" + curr_doc[0] + "-" + curr_doc[1]).removeClass().addClass("annotated enabled current " + scale[new_val]);
    }

    data[curr_doc[0]][curr_doc[1]]["TH"] = vth;
    data[curr_doc[0]][curr_doc[1]]["SP"] = vsp;

    $.getJSON('/set/' + curr_doc[0] + "/" + curr_doc[1] + "/" + vth + "/" + vsp /*+ "/" + vtp*/ , {},
      function(data) {
        if (data["status"] != "OK") {
          alert("error while saving !");
        }
      });
  }

  $(document).keydown(function(event) {
    if (event["key"] == "Enter") {
      update_val();
    }
  });

  $("#annotate_btn").click(function() {
    update_val();
  });


  $(".annotated.enabled").click(function() {
    $(".current").removeClass("current");
    $(this).addClass("current");
    var id = $(this).attr("id").split("-");
    curr_doc = id;

    $.getJSON('/get_doc/' + id[0], {},
      function(data) {
        $("#title_doc1").html("<span class='label label-default'>N°" + id[0] + "</span> " + data[0]);
        $("#doc_1").text(data[1]);
      });
    $.getJSON('/get_doc/' + id[1], {},
      function(data) {
        $("#title_doc2").html("<span class='label label-default'>N°" + id[1] + "</span> " + data[0]);
        $("#doc_2").text(data[1]);
      });
    $("#TH .btn").removeClass("activee");
    $("#SP .btn").removeClass("activee");
    $("#TH .btn." + scale[data[curr_doc[0]][curr_doc[1]]['TH']]).addClass("activee");
    $("#SP .btn." + scale[data[curr_doc[0]][curr_doc[1]]['SP']]).addClass("activee");
    return false;
  });

  function hideAll() {
    $('.popup-open').each(function(index) {
      $(this).remove();
    });
  };
  var t;
  $('.annotated.enabled').mouseover(

    function(e) {
      var id = $(this).attr("id").split("-");
      var vth = scale[data[id[0]][id[1]]["TH"]];
      var vsp = scale[data[id[0]][id[1]]["SP"]];
      hideAll();
      clearTimeout(t);

      $(this).append("<div class=\"popup-open\"><p>" + id[0] + "-" + id[1] + "</p><table><tr><td class=\"" + vth + "\">TH</td><td class=\"" + vsp + "\">SP</td></tr></table></div>"); //<td class=\""+vtp+"\">TP</td>
      var pos = $(".popup-open").position();
      console.log(pos);
      $(".popup-open").attr("style", "left:" + (pos.left - 50).toString() + "px;top:" + (pos.top - 50).toString() + "px;")
      console.log($(".popup-open").attr("style"));
    }
  ).mouseout(

    function(e) {

      t = setTimeout(function() {
        hideAll();
      }, 300);

    }
  );
});
$(function() {
  $(".annotate .btn").click(function() {
    var _type = $(this).parent().attr("id");
    var _val = parseInt($(this).attr("val"));

    $("#" + _type + " .btn").removeClass("activee");
    $(this).addClass("activee");

  });
  $("#annotate_btn").click(function() {

  });
});
