# -*- coding: utf-8 -*-

import argparse,json,glob,re,os
import numpy as np

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("input", help="Input filename")
parser.add_argument("sub_corpus_size", help="sub corpus size")
parser.add_argument("output", help="Output filename")
args = parser.parse_args()

corpus=json.load(open("corpus/epidemio.json"))
data=corpus["data"]

sub_corpus_size=10
size=len(data)//sub_corpus_size

sub_groups = np.array_split(np.array(data),size)
sub_groups = [len(a) for a in sub_groups]

groups,t=[],0
for i in range(len(data)):
    for j in range(len(data)):
        if j > i:
            groups.append([i,j])

nb_of_docs=(sub_corpus_size*(sub_corpus_size-1))/2
size=len(groups)//nb_of_docs
sub_groups = np.array_split(np.array(groups),size)
np.unique(sub_groups[0].flatten())
len(sub_groups)
os.makedirs("generated_corpus/")
