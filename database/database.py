# -*- coding: utf-8 -*-

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String, Boolean, ForeignKey, Text
from sqlalchemy.orm import sessionmaker

import bcrypt


engine = create_engine('sqlite:////Users/jacquesfize/db/test.db', echo=True)
Base = declarative_base()

class Corpus(Base):
    __tablename__="corpora"

    id = Column(Integer, primary_key=True)
    title = Column(String(80), unique=True)
    file_path = Column(String(120), unique=True)
    part_of =  Column(Integer, ForeignKey('corpora.id'))

    def __init__(self, title,path,part_of=None):
        self.title=title
        self.file_path=path
        self.part_of=part_of


class User(Base):
    __tablename__="users"

    id = Column(Integer, primary_key=True)
    name = Column(String(80), unique=True)
    email = Column(String(120), unique=True)
    password = Column(String(120))
    level = Column(Integer)

    def __init__(self, name, email,password,level=1,sign_up=True):
        self.name=name
        self.email=email
        self.password= password
        if sign_up:self.password=bcrypt.hashpw(password, bcrypt.gensalt(14))
        self.level=level


    def check_password(self,password):
        return self.password == bcrypt.hashpw(password.encode(), self.password)
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

class Annotation(Base):
    __tablename__="annotations"

    id = Column(Integer, primary_key=True)
    corpus_id = Column(Integer,ForeignKey('corpora.id'))
    user_id = Column(Integer,ForeignKey('users.id'))
    data=Column(Text)
    finished = Column(Boolean)

    def __init__(self, corpus_id,user_id,data,finished=False):
        self.corpus_id=corpus_id
        self.user_id=user_id
        self.finished=finished
        self.data=data
###################################################
#              Database Population functions
###################################################

def add_users(session,data):
    for d in range(len(data)):
        line=data.iloc[[d]].values[0]
        user=User(line[0],line[1],line[2].encode(),line[3])
        session.add(user)
    session.commit()
def add_corpora(session,data):
    for d in range(len(data)):
        line=data.iloc[[d]].values[0]
        user=Corpus(line[0],line[1],line[2])
        session.add(user)
    session.commit()

if __name__ == '__main__':
    # Create tables (delete if exists)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # Initialize session
    Session = sessionmaker(bind=engine)
    session = Session()

    # Load data
    user_input=pd.read_csv("user.csv",sep=";")
    corpora_input=pd.read_csv("corpus.csv",sep=";")
    # Populate the database
    add_users(session,user_input)
    add_corpora(session,corpora_input)
