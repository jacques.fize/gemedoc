# Gemedoc
We present **Gemedoc**, a platform for text similarity annotation based on the spatial, the thematic, and ultimately the temporal dimensions. To this end, a two-step annotation protocol was designed to assess the similarity between two documents: (1) identification of salient features according to the two analysis dimensions; (2) similarity assessment according to a 4-degree scale. Ultimately, the labeled data retrieved from different corpora could be used as benchmark for text-mining applications.

## Database
### Initialize Database

First, create a user.csv, which contains users data. It should have this syntax:

    name;email;password;level
    Admin;admin@gmail.com;admin;1

Then, initialize the user database, run the next script:

  $ python3 database/database.py


#### Admin user:
 * email: admin@gmail.com
 * password: admin

### Add User

Sign in as an Administrator, then go to : {server.name}/signup

## Run the server

    $ python3 server.py


# Credits

Jacques Fize, jacques.fize[at]cirad[dot]fr, 2017
