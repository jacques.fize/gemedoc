# TODO

# 3 octobre

 * Add text + similarity viewer ? **Done**
 * Other measure of quality ? **Reported**
 * Average visualisation of the results generated by the users ? **Done**
 *

## 29 sept
 * Add "New Annotation" button + modal --> **Done** (but change the ways of doing --> Select Corpus button)
 * Change "Load Annotation" --> modal that shows current annotation for a user --> **Done** --> Automatically loaded when selected corpus
 * Add "Add user page ?" --> **Done**
