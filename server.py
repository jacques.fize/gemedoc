# coding=utf-8
import os, json, re, datetime, random, uuid
from flask import Flask, render_template, url_for, flash, make_response, request, redirect, session, Markup, jsonify
from flask_session import Session
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from database.database import *
from statistical.data_manipulation import *
from statistical.convergence_op import *
from sqlalchemy import update
app = Flask(__name__)

###################################################
#              Configuration variables
###################################################

# Upload configuration
UPLOAD_FOLDER = "."
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Database configuration
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://jacobe:kitebgm1@localhost/matching_event'
#app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
Sessiona = sessionmaker(bind=engine)
sql_session = Sessiona()

# Session configuration
SESSION_TYPE = 'filesystem'
PERMANENT_SESSION_LIFETIME = datetime.timedelta(days=1)

# Similarity Scale configuration
list_class = ["unkown", "minus", "plus", "plusplus"]

app.config.from_object(__name__)

# Initialize Session manager
Session(app)

# Initialize the Login Manager
login_manager = LoginManager()
login_manager.init_app(app)

# Database manager
#db = SQLAlchemy(app)


###################################################
#                   Routes functions
###################################################

@app.route('/')
@login_required
def index():
    """
    Index page
    """
    # initialize phase
    if not "corpus" in session:
        session["corpus_id"]=1
        set_corpus(1)

    if not session["data"]:
        session["data"] = generate_empty_annotation_grid(
            len(session["corpus"]))

    # if everything settled
    i_, j_ = session["documents_order"][session["curr"]][0], session["documents_order"][session["curr"]][1]
    if finished():
        return render_template('index.html', corpus=session["corpus"], class_matrix=session["data"], scale=list_class, i=i_, j=j_, finished=True)
    return render_template('index.html', corpus=session["corpus"], class_matrix=session["data"], scale=list_class, i=i_, j=j_)


@app.route("/documentation")
def documentation():
    """
    Display the procedure page
    """
    return render_template("documentation.html")

@app.route("/about")
def about():
    """
    Display the about gemedoc page
    """
    return render_template("about.html")


@app.route("/example")
def example():
    """
    Display the example page
    """
    return render_template("example.html")

@app.route("/corpus")
@login_required
def corpus():
    """
    Display the example page
    """
    return render_template("corpus_explorer.html",corpus_id=range(len(session["corpus"])))


@app.route("/next")
@login_required
def next():
    """
    Move to the next couple of documents to annotate
    """
    if session["curr"] + 1 < len(session["documents_order"]):
        session["curr"] += 1
    return redirect("/")


@app.route("/previous")
@login_required
def previous():
    """
    Move to the previous couple of documents to annotate
    """
    if session["curr"] - 1 >= 0:
        session["curr"] -= 1
    return redirect("/")


@app.route('/visu')
@app.route('/visu/<type_>')
@login_required
def visualize(type_="SP"):
    """
    Annotation visualization page
    """
    data=sql_session.query(Annotation).filter_by(corpus_id=session["corpus_id"]).all()
    mats=[transform2matrix(json.loads(d.data),type_) for d in data]
    return render_template('grid.html',
            kappa_cohen=0,
            kappa_fleiss=fleiss_kappa(*mats) if len(mats)>2 else 0,
            friedman=friedman(*mats).statistic if len(mats)>2 else 0,
            mat=get_annotation_matrix(current_user.id,session["corpus_id"],type_)
             )


@app.route("/loadannotated", methods=['GET', 'POST'])
@login_required
def upload_annotated():
    """
    Load user annotation
    """
    if request.method == 'POST':
        # Check if the post request has the file part
        if 'file_a' not in request.files:
            return redirect("/")
        # Get the file information
        file = request.files['file_a']
        # If no file selected
        if file.filename == '':
            return redirect("/")
        # Else
        path_=os.path.join(app.config['UPLOAD_FOLDER'], "{0}.txt".format(uuid.uuid4()))
        file.save(path_) # Save the uploaded file
        load_annotated_data(path_) # Load the data inside
        os.remove(path_) # Remove the temp file
        return redirect('/')


@app.route("/loadcorpus", methods=['GET', 'POST'])
@login_required
def upload_corpus():
    """
    Load a corpus
    """
    if request.method == 'POST':
        # Check if the post request has the file part
        if 'file' not in request.files:
            return redirect("/")
        # Get the file information
        file = request.files['file']
        # If no file selected
        if file.filename == '':
            return redirect("/")
        # Else
        path_=os.path.join(app.config['UPLOAD_FOLDER'], "{0}.txt".format(uuid.uuid4()))
        file.save(path_) # Save the uploaded file
        load_corpus(path_) # Load the data inside
        os.remove(path_) # Remove the temp file
        return redirect('/')


@app.route("/save")
@login_required
def save(flash_=True):
    """
    Save the user annotation data
    """
    data = session["data"]
    data=json.dumps(data, indent=4, ensure_ascii=False)
    anno=sql_session.query(Annotation).filter_by(corpus_id=session["corpus_id"],user_id=current_user.id).first()
    if not anno:
        sql_session.add(Annotation(session["corpus_id"],current_user.id,data,finished()))
    else:
        anno.data=data
    sql_session.commit()
    if flash_:
        flash(Markup("Your annotation has been saved for this corpus"), 'success')
    return redirect("/")


@app.route("/get_doc/<int:id_>")
@login_required
def get_doc(id_):
    """
    Return the document with the id == "id_"
    """
    return json.dumps(session["corpus"][id_])

@app.route("/set/corpus/<int:id_>")
@login_required
def set_corpus(id_):
    """
    Return the corpus list
    """
    data=sql_session.query(Corpus).filter_by(id=id_).first()
    session["corpus_id"]=data.id
    load_corpus(data.file_path)
    return(redirect("/"))

@app.route("/get_corpus")
@login_required
def get_corpus():
    """
    Return the corpus list
    """
    data=sql_session.query(Corpus).all()
    response=[[d.id,d.title] for d in data]

    return json.dumps(response)



@app.route("/clear")
@login_required
def clear():
    session["data"] = generate_empty_annotation_grid(len(session["corpus"]))
    save(flash_=False)
    return redirect("/")


@app.route('/set/<int:id1>/<int:id2>/<int:vth>/<int:vsp>')  # "/<int:vtp>')
@login_required
def set_annotation(id1, id2, vth, vsp):
    """
    Update annotation value between two documents
    """

    try:
        session["data"][id1][id2] = {"SP": vsp, "TH": vth, "annotated": 1}
        session["data"][id2][id1] = {"SP": vsp, "TH": vth, "annotated": 1}
        save(flash_=False)
        return json.dumps({"status": "OK"})
    except:
        return json.dumps({"status": "ERROR"})


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    User login
    """
    # If already logged in
    if current_user.is_authenticated:
        return redirect("/")
    # Login page render
    if request.method == 'GET':
        return render_template('login.html')

    # Get necessary variable
    email = request.form['email']
    password = request.form['password']
    registered_user = sql_session.query(User).filter_by(email=email).first()

    # Error message
    error = Markup(
        '<strong>Email</strong> or <strong>Password</strong> is invalid')
    # If no user found
    if registered_user is None:
        flash(Markup(error), 'error')
        return redirect(url_for('login'))
    # If password is incorrect
    if not registered_user.check_password(password):
        flash(error, 'error')
        return redirect(url_for('login'))
    # Logged the user
    login_user(registered_user)

    return redirect(request.args.get('next') or url_for('index'))

@app.route('/signup', methods=['GET', 'POST'])
@login_required
def signup():
    """
    User signup
    """
    # If already logged in
    if current_user.level != 1:
        return redirect("/")
    # Login page render
    if request.method == 'GET':
        return render_template('signup.html')

    # Get necessary variable
    name = request.form['name']
    email = request.form['email']
    password = request.form['password']
    password2 = request.form['password_2']
    if name and email and password:
        if password == password2:
            user=User(name,email,password.encode(),1)
            sql_session.add(user)
            sql_session.commit()
            flash(Markup('Account for {0} is created !'.format(name)), 'success')
        else:
            error = Markup('Indicate two identical password !')
            flash(error, 'danger')
    else:
        error = Markup('<strong>Email</strong> or <strong>Password</strong> or <strong>Name</strong> is empty')
        flash(error, 'danger')
    return redirect("/signup")

@app.route('/logout')
def logout():
    """
    Logout page
    """
    logout_user()
    session.clear()
    return redirect("/")

###################################################
#         Login Manager Functions Overrided
###################################################

@login_manager.user_loader
def load_user(id):
    return sql_session.query(User).get(int(id))


@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect("/login")

###################################################
#                   Hidden Functions
###################################################

def load_annotated_data(fn):
    """
    Load annotion data
    """
    session["data"] = json.load(open(fn))


def load_corpus(fn):
    """
    Load corpus
    """
    session["corpus"] = json.load(open(fn))
    session["title"] = session["corpus"]["title"]
    session["corpus"] = session["corpus"]["data"]
    row=sql_session.query(Annotation).filter_by(user_id=current_user.id,corpus_id=session["corpus_id"]).first()
    n = len(session["corpus"])
    print("len_",n)
    if not row:
        session["data"] = generate_empty_annotation_grid(n)
    else:
        session["data"] = json.loads(row.data)


    session["documents_order"] = []
    for i in range(n):
        for j in range(n):
            if i != j and j > i : #and session["data"][i][j]["annotated"] !=1:
                session["documents_order"].append([i, j])
    random.shuffle(session["documents_order"])
    print(session["documents_order"])
    session["curr"] = 0


def generate_empty_annotation_grid(n):
    """
    Generate an empty annotation grid
    """
    return [[{"SP": 0, "TH": 0, "annotated": 0} for j in range(n)] for i in range(n)]


def get_annotation_matrix(user_id,corpus_id,dimension="SP"):
    data = sql_session.query(Annotation).filter_by(user_id=user_id,corpus_id=corpus_id).first().data
    data=json.loads(data)
    matrix_ = transform2matrix(data,dimension)
    return matrix_.tolist()

def finished():
    """
    Return true, if all document couples were annotated
    """
    for i in session["documents_order"]:
        if session["data"][i[0]][i[1]]["annotated"] == 0:
            return False
    return True

if __name__ == '__main__':
    port = 5000 + 890
    url = "http://127.0.0.1:{0}".format(port)
    app.secret_key = os.urandom(24)
    app.run(host='0.0.0.0', port=port, debug=True)
